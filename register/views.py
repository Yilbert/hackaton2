import csv
import locale
import pathlib
from datetime import datetime

import pandas as pd
from django.shortcuts import render

from . import models
from .models import Bank, InputCsv, Order, OrdersFile
from .utils.clean_xlsx_data import read_xlsx


# Create your views here.

def list_register(request):
    registros = OrdersFile.objects.all()
    return render(request, 'list_register.html', {'registros': registros})


def upload_file(request):
    des_val = []
    descrip = []
    year_val = []
    msg = ''
    documents = models.Bank.objects.all
    objeto = []
    num = 0
    if request.method == "POST":
        ext = ['.csv']
        upload_files = request.FILES['upload']

        extension = pathlib.Path(upload_files.name).suffix

        if extension in ext:
            document = models.InputCsv(
                uploadedFile=upload_files
            )
            document.save()
            documents = models.Bank.objects.all()
        else:
            msg = 'Archivo No valido'
        with open(document.uploadedFile.path, newline='') as File:
            # if request.method == 'POST':
            datos = []
            reader = csv.reader(File)
            for row in reader:
                datos.append(row)
            fields = datos[0]
            datos.pop(0)

            for date in datos:
                objeto_values = dict(zip(fields, date))
                objeto.append(objeto_values)
            locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

            valor = []
            valor2 = []
            year = []
            for obj in objeto:
                date = datetime.strptime(obj.get('Date'), '%d-%b-%Y')
                if obj.get('Description') not in descrip:
                    descrip.append(obj.get("Description"))
                if date.year not in year:
                    year.append(date.year)

                obj = Bank.objects.create(date=datetime.strptime(obj.get('Date'), '%d-%b-%Y'),
                                          description=obj.get('Description'),
                                          deposits=locale.atof(obj.get('Deposits')),
                                          withdrawls=locale.atof(obj.get('Withdrawls')),
                                          balance=locale.atof(obj.get('Balance')))

            obj.save()
            num = len(objeto)
            df = pd.DataFrame(objeto)
            df['Date'] = pd.to_datetime(df.Date, format='%d-%b-%Y')
            df['year'] = pd.DatetimeIndex(df['Date']).year
            for desc in descrip:
                valor.append(len(df[df.Description.eq(desc)]))
            for anio in year:
                valor2.append(len(df[df.year.eq(anio)]))
            for i in range(len(descrip)):
                des_val.append(dict(zip(('name', 'value'), (descrip[i], valor[i]))))
            for i in range(len(year)):
                year_val.append(dict(zip(('date', 'value2'), (year[i], valor2[i]))))

    return render(request, "input_file.html",
                  context={'files': documents, 'msg': msg, 'datas': objeto, 'num': num, 'dict': des_val,
                           'year': year_val}, )


# For the xlsx file
def xlsx_read(request):
    msg = ''
    documents = OrdersFile.objects.all()
    context = {
        'file_data': '',
        'col_names': '',
    }
    if request.method == "POST":
        ext = ['.xlsx']
        file_title = request.POST["fileTitle"]
        upload_files = request.FILES['uploadedFile']

        extension = pathlib.Path(upload_files.name).suffix

        if extension in ext:
            document = OrdersFile(
                name=file_title,
                uploadedFile=upload_files,
            )
            document.save()

            if document:
                # The function read_xlsx is created in ./utils/clean_xlsx_data.py
                [data, col_names] = read_xlsx(document.uploadedFile.path)
                for obj in data:
                    Order.objects.create(region=obj.get('Region'), country=obj.get('Country'),
                                         item_type=obj.get('ItemType'), sales_channel=obj.get('SalesChannel'),
                                         order_priority=obj.get('OrderPriority'), order_date=obj.get('OrderDate'),
                                         order_id=obj.get('OrderID'), ship_date=obj.get('ShipDate'),
                                         units_sold=obj.get('UnitsSold'), unit_price=obj.get('UnitPrice'),
                                         unit_cost=obj.get('UnitCost'), total_revenue=obj.get('TotalRevenue'),
                                         total_cost=obj.get('TotalCost'), total_profit=obj.get('TotalProfit'))

            # Clean rows Number
            file_clean_data = Order.objects.all()
            clean_rows = file_clean_data.count()

            # Items type
            item_types_list = Order.objects.values('item_type').distinct()

            # Number and type of Regions
            regions = Order.objects.values('region').distinct()
            regions_count = regions.count()

            # Orders count according to the priority
            order_priorities_list = Order.objects.values('order_priority').distinct()
            order_priorities_count = {}
            for priority in order_priorities_list:
                priority_value = priority.get('order_priority')
                order_priorities_count[priority_value] = Order.objects.filter(order_priority=priority_value).count()

            # Orders count according to the item type
            item_types_count = {}
            for types in item_types_list:
                type_value = types.get('item_type')
                item_types_count[type_value] = Order.objects.filter(item_type=type_value).count()

            context = {
                'file_data': file_clean_data,
                'col_names': col_names,
                'count_rows': clean_rows,
                'items_type': item_types_list,
                'regions_count': regions_count,
                'regions': regions,
                'orders_priority_count': order_priorities_count,
                'item_type_count': item_types_count,

            }

    return render(request, 'load_xlsx.html', context)
