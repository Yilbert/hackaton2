import re
import pandas as pd
from datetime import datetime


def clean_data(col_name, cell):
    # Note: All the nan values are converted to None

    # Validating string values
    str_type_cols = ['Region', 'Country', 'ItemType', 'SalesChannel', 'OrderPriority']
    if col_name in str_type_cols:
        if type(cell) is not str:
            cell = None

    # Setting up the date format
    date_type_cols = ['OrderDate', 'ShipDate']
    if col_name in date_type_cols:
        if type(cell) != datetime:
            try:
                cell = datetime.strptime(cell, "%m/%d/%Y")
            except TypeError:
                cell = None

    # Validating integer values
    integer_type_cols = ['OrderID', 'UnitsSold']
    if col_name in integer_type_cols:
        regex = r"^[0-9]*$"
        is_integer = re.match(regex, str(cell)) is not None
        if not is_integer:
            cell = None

    # Validating float values
    float_type_cols = ['UnitPrice', 'UnitCost', 'TotalRevenue', 'TotalCost', 'TotalProfit']
    if col_name in float_type_cols:
        regex = r"^[+]?([0-9]+(\.[0-9]+)?|\.[0-9]+)$"
        is_float = re.match(regex, str(cell)) is not None
        if not is_float:
            cell = None

    return cell


def read_xlsx(data_path):
    # Reading the file
    data = pd.read_excel(data_path)  # [:100]
    rows_number = len(data.index)

    # Extracting the columns names
    cols_names = []
    original_cols_names = []
    for cols in data:
        original_cols_names.append(cols)
        cols = cols.replace(" ", "")
        cols_names.append(cols)

    # To list of dictionaries
    data_dict = []
    for i in range(rows_number):
        row = {}
        for j in range(len(cols_names)):
            cell = clean_data(cols_names[j], data.iat[i, j])
            row[cols_names[j]] = cell
            if cell is None:
                break

        if not (None in row.values()):
            data_dict.append(row)

    return [data_dict, original_cols_names]
