from django.urls import path
from register import views

urlpatterns = [
    path('', views.upload_file, name='input_files'),
    path('load_xlsx/', views.xlsx_read, name='load_xlsx'),
    path('list_register/', views.list_register, name='list_register')
]
